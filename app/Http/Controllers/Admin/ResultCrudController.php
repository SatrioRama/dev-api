<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ResultRequest;
use App\Models\Device;
use App\Models\Vendor;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ResultCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ResultCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Result::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/result');
        CRUD::setEntityNameStrings('result', 'results');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'     => 'vendor_id',
            'label'    => 'Vendor',
            'type'     => 'closure',
            'function' => function($entry) {
                if (!empty($entry->vendor_id)) {
                    return Vendor::find($entry->vendor_id)->name;
                }else {
                    return "";
                }
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'device_id',
            'label'    => 'Device',
            'type'     => 'closure',
            'function' => function($entry) {
                if (!empty($entry->device_id)) {
                    return Device::find($entry->device_id)->device_name;
                }else{
                    return "";
                }
            }
        ]);
        $this->crud->addColumn([
            'name'  => 'ip_address',
            'label' => 'IP Address',
            'type'  => 'text'
        ]);
        CRUD::setFromDb(); // columns
        $this->crud->addColumn('created_at');
        $this->crud->removeAllButtons();
        $this->crud->enableExportButtons();
        $this->crud->enableResponsiveTable();
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ResultRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
