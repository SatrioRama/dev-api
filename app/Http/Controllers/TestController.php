<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\Test;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Passport;

class TestController extends Controller
{
    public function authten(Request $request)
    {
        $now = Carbon::now();
        $body = json_decode($request->getContent());
        dd($request->getContent());
        $vendor = Vendor::where('username', '=', $body->username)->first();
        if (!empty($vendor)) {
            if (Hash::check($body->password, $vendor->password)) {
                $vendor->createToken($now->toDateTimeString().$vendor->name);
                $tokenTable = DB::table('personal_access_tokens')->where('tokenable_id','=',$vendor->id)->orderBy('created_at', 'desc')->first();
                return response()->json([
                    'status' => 'OK',
                    'message' => 'username found',
                    'token' => $tokenTable->token,
                ], 200);
            }else {
                return response()->json([
                    'status' => 'OK',
                    'message' => 'username found wrong password',
                ], 200);
            }

        }else {
            return response()->json([
                'data' => 'username not found'
            ], 404);
        }
    }

    public function aqmdata(Request $request)
    {
        $time = Carbon::now();
        $body = json_decode($request->getContent());
        if (!empty($request->bearerToken())) {
            $token = DB::table('personal_access_tokens')->where('token','=',$request->bearerToken())->orderBy('created_at', 'desc')->first();
            if (!empty($token)) {
                $vendor = Vendor::find($token->tokenable_id);
                if (!empty($vendor)) {
                    if (!empty($body)) {
                        $device = Device::where('device_id', '=', $body->id_stasiun)->first();
                        if (!empty($device)) {
                            $data = new Test();
                            $data->vendor_id = $vendor->id;
                            $data->ip_address = $request->ip();
                            $data->device_id = $device->id;
                            $data->waktu = $body->waktu;
                            $data->pm10 = (!empty($body->pm10)) ? $body->pm10 : null;
                            $data->pm25 = (!empty($body->pm25)) ? $body->pm25 : null;
                            $data->so2 = (!empty($body->so2)) ? $body->so2 : null;
                            $data->co = (!empty($body->co)) ? $body->co : null;
                            $data->o3 = (!empty($body->o3)) ? $body->o3 : null;
                            $data->no2 = (!empty($body->no2)) ? $body->no2 : null;
                            $data->hc = (!empty($body->hc)) ? $body->hc : null;
                            $data->ws = (!empty($body->ws)) ? $body->ws : null;
                            $data->wd = (!empty($body->wd)) ? $body->wd : null;
                            $data->stat_pm10 = (@$body->stat_pm10 !== null) ? @$body->stat_pm10 : null;
                            $data->stat_pm25 = (@$body->stat_pm25 !== null) ? @$body->stat_pm25 : null;
                            $data->stat_so2 = (@$body->stat_so2 !== null) ? @$body->stat_so2 : null;
                            $data->stat_co = (@$body->stat_co !== null) ? @$body->stat_co : null;
                            $data->stat_o3 = (@$body->stat_o3 !== null) ? @$body->stat_o3 : null;
                            $data->stat_no2 = (@$body->stat_no2 !== null) ? @$body->stat_no2 : null;
                            $data->stat_hc = (@$body->stat_hc !== null) ? @$body->stat_hc : null;
                            $data->humidity = (!empty($body->humidity)) ? $body->humidity : null;
                            $data->temperature = (!empty($body->temperature)) ? $body->temperature : null;
                            $data->preasure = (!empty($body->pressure)) ? $body->pressure : null;
                            $data->sr = (!empty($body->sr)) ? $body->sr : null;
                            $data->rain_intensity = (!empty($body->rain_intensity)) ? $body->rain_intensity : null;
                            $data->save();

                            return response()->json([
                                "status" => 'success',
                                "message" => 'success input data',
                                "waktu" => $time->toDateTimeString()
                            ], 200);
                        }else {
                            return response()->json([
                                "status" => 'fail',
                                "message" => 'device not found',
                            ], 404);
                        }
                    } else {
                        return response()->json([
                            "status" => 'fail',
                            "message" => 'body data is null',
                        ], 404);
                    }
                }else {
                }
                dd($request->bearerToken(), $token->tokenable_id);
            }else {
                return response()->json([
                    "status" => 'fail',
                    "message" => 'token not found',
                ], 404);
            }
            $body = json_decode($request->getContent());
        }else {
            return response()->json([
                "status" => 'fail',
                "message" => 'no token on header',
            ], 401);
        }
    }

    public function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); // it will return the server IP if the client IP is not found using this method.
    }
}
