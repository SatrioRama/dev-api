<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('result') }}'><i class='nav-icon la la-question'></i> Results</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('device') }}'><i class='nav-icon la la-question'></i> Devices</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('vendor') }}'><i class='nav-icon la la-question'></i> Vendors</a></li>