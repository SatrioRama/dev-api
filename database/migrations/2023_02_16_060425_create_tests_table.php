<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->id();
            $table->integer('vendor_id')->nullable();
            $table->integer('device_id')->nullable();
            $table->dateTime('waktu')->nullable();
            $table->float('pm10')->nullable();
            $table->float('pm25')->nullable();
            $table->float('so2')->nullable();
            $table->float('co')->nullable();
            $table->float('o3')->nullable();
            $table->float('no2')->nullable();
            $table->float('hc')->nullable();
            $table->float('ws')->nullable();
            $table->float('wd')->nullable();
            $table->integer('stat_pm10')->nullable();
            $table->integer('stat_pm25')->nullable();
            $table->integer('stat_so2')->nullable();
            $table->integer('stat_co')->nullable();
            $table->integer('stat_o3')->nullable();
            $table->integer('stat_no2')->nullable();
            $table->integer('stat_hc')->nullable();
            $table->float('humidity')->nullable();
            $table->float('temperature')->nullable();
            $table->float('preasure')->nullable();
            $table->float('sr')->nullable();
            $table->float('rain_intensity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
