<?php

namespace Database\Seeders;

use App\Models\Device;
use Illuminate\Database\Seeder;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $device1 = Device::create([
            'device_id' => '4a674ad1-d81c-11ec-a258-41883fa319e0',
            'device_name' => 'ANG5-00322',
        ]);

        $device2 = Device::create([
            'device_id' => '4087b1f0-d4fa-11ec-a502-79978f9d7342',
            'device_name' => 'ANG5-00333',
        ]);

        $device3 = Device::create([
            'device_id' => '40884e30-d4fa-11ec-a502-79978f9d7342',
            'device_name' => 'ANG5-00334',
        ]);

        $device3 = Device::create([
            'device_id' => 'UJI_PT_ATT2',
            'device_name' => 'Device Uji Conectivity',
        ]);
    }
}
