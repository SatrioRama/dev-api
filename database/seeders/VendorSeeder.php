<?php

namespace Database\Seeders;

use App\Models\Vendor;
use Illuminate\Database\Seeder;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = Vendor::create([
            'name' => 'PT. Anomali Lintas Teknologi',
            'username' => 'pt_anomali_lintas_teknologi_last',
            'password' => bcrypt('QN%Kr3H?ejz8&7F1')
        ]);

        $user2 = Vendor::create([
            'name' => 'PT. Anomali Trans Teknologi',
            'username' => 'pt_anomali_trans_teknologi_new',
            'password' => bcrypt('QN%Kr3H?ejz8&7FZ')
        ]);

        $user3 = Vendor::create([
            'name' => 'PT. Anomali Trans Teknologi',
            'username' => 'pt_anomali_trans_teknologi',
            'password' => bcrypt('ZvK7dXaMdYd2T&c8')
        ]);
    }
}
